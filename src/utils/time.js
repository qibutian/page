function formatDate(value, type) {
    let date = new Date(value);
    let y = date.getFullYear();
    let MM = date.getMonth() + 1;
    MM = MM < 10 ? "0" + MM : MM;
    let d = date.getDate();
    d = d < 10 ? "0" + d : d;
    let h = date.getHours();
    h = h < 10 ? "0" + h : h;
    let m = date.getMinutes();
    m = m < 10 ? "0" + m : m;
    let s = date.getSeconds();
    s = s < 10 ? "0" + s : s;
    if (type == 1) {
        return y + "-" + MM + "-" + d
    } else if (type == 2) {
        return y + "年" + MM + "月" + d + "日"
    }
    return y + "/" + MM + "/" + d + " " + h + ":" + m
}

export {

    formatDate

}
