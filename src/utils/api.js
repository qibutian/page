let BASE_URL = "http://47.98.169.218:8082";
export default {
  baseUrl: BASE_URL,
//登录
  checkToken: `POST ${BASE_URL}/wechat/user/checkToken`,
  sendSms: `POST ${BASE_URL}/wechat/user/sendSms`,
  wxLogin: `POST ${BASE_URL}/wechat/user/wxLogin`,
  exchangePhone: `POST ${BASE_URL}/wechat/user/exchangePhone`,//交换电话
   findMyExchange: `${BASE_URL}/wechat/user/findMyExchange`, //获取交换电话的数据列表
   findMyExchangeCount: `${BASE_URL}/wechat/user/findMyExchangeCount`, //获取未处理交换电话的数量
   hxRegisterFriend: `POST ${BASE_URL}/wechat/user/hxRegisterFriend`, //加好友
   indexa: `${BASE_URL}/wechat/user/index`,//通讯录首页
   industryList: `${BASE_URL}/wechat/user/industryList`,//获取所有行业
   msgCount: `${BASE_URL}/wechat/user/msgCount`,//未读消息
msgForShare: `${BASE_URL}/wechat/user/msgForShare`,//获取分享的提示内容
   msgRecord: `POST ${BASE_URL} /wechat/user/msgRecord`,//消息存留
   obtainCoupon: `${BASE_URL}/wechat/user/obtainCoupon`,//获取红包
  resourceList: `${BASE_URL}/wechat/user/resourceList`, //获取所有资源(后台系统使用)
   search: `${BASE_URL}/wechat/user/search`, //通讯录人员列表
   shareRecord: `${BASE_URL}/wechat/user/shareRecord`,//获取邀请记录
   share :`${BASE_URL}/wechat/user/share`, //获取分享参数
  updateBackInfo: `POST ${BASE_URL}/wechat/user/updateBackInfo`,//编辑用户信息后太实用
  updateExchangePhone: `POST ${BASE_URL} /wechat/user/updateExchangePhone`, //同意或者拒绝交换电话
  updateInfo: `POST ${BASE_URL}/wechat/user/updateInfo`, //编辑用户信息
  waterUserList: `${BASE_URL}/wechat/user/waterUserList`, //获取我的流水
   wxHeadPic: `POST ${BASE_URL}/wechat/user/wxHeadPic`, //获取用户头像
    wxInfo: `${BASE_URL}/wechat/user/wxInfo`,//获取用户信息
  wxInfoForLogin: `${BASE_URL}/wechat/user/wxInfoForLogin`,//登录时获取当前用户的微信头像和昵称,

};
